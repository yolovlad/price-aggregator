//
//  AppDelegate.m
//  NIXProject
//
//  Created by Egor Zubkov on 1/22/13.
//  Copyright (c) 2013 NIX. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#if (defined(CONFIGURATION_Debug))
    #import <SDStatusBarManager.h>
#endif

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window = window_;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#if (defined(CONFIGURATION_Debug))
    
    #if TARGET_IPHONE_SIMULATOR
    {
        [[SDStatusBarManager sharedInstance] enableOverrides];
    }
    #endif
    
#endif
    
    [Fabric with:@[[Crashlytics class]]];
    
    return YES;
}

@end
