//
//  main.m
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
