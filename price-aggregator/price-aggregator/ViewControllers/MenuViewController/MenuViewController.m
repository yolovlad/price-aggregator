//
//  MenuViewController.m
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "MenuViewController.h"

@implementation MenuViewController
{
    NSArray *menuItems;
    NSIndexPath *selectedCellOnIndex;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self hightlightCurrentSection];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    menuItems = @[@"search", @"favorites", @"shops"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:0.33 green:0.33 blue:0.33 alpha:1.0];
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:selectedCellOnIndex animated:YES];
    selectedCellOnIndex= indexPath;
}

#pragma self methods
-(void)hightlightCurrentSection
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];

    if (selectedCellOnIndex)
    {
        [[self tableView]selectRowAtIndexPath:selectedCellOnIndex animated:NO scrollPosition:UITableViewScrollPositionNone];
    }else
    {
        [[self tableView]selectRowAtIndexPath:indexPath
                                     animated:NO
                               scrollPosition:UITableViewScrollPositionNone];
    }
}

@end
