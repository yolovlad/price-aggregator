//
//  FavoriteViewController.m
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "FavoriteViewController.h"
@interface FavoriteViewController()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

@end
@implementation FavoriteViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addRevealViewController];
}

-(void)addRevealViewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [[self sidebarButton ] setTarget: self.revealViewController];
        [[self sidebarButton ]setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
    [revealViewController setRightViewController:nil];

}
@end
