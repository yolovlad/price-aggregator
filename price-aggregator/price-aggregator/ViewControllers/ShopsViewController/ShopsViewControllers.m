
#import "ShopsViewControllers.h"
@interface ShopsViewControllers()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;

-(void)addRevealVciewController;

@end
@implementation ShopsViewControllers
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addRevealVciewController];
}

-(void)addRevealVciewController
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [[self sidebarButton ] setTarget: self.revealViewController];
        [[self sidebarButton ]setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    }
    [revealViewController setRightViewController:nil];
}

@end
