//
//  SearchViewController.m
//  price-aggregator
//
//  Created by Yalovenko on 29.09.16.
//  Copyright © 2016 Yalovenko. All rights reserved.
//

#import "SearchViewController.h"
@interface SearchViewController()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *side;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingButton;

@end
@implementation SearchViewController
{
    UIViewController * sortingViewController;
}

-(void)awakeFromNib
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    sortingViewController = [storyboard instantiateViewControllerWithIdentifier:@"SortingVC"];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [[self side ] setTarget: self.revealViewController];
        [[self side ]setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
        [revealViewController setRightViewController:sortingViewController];
        [[self settingButton ] setTarget: self.revealViewController];
        [[self settingButton ]setAction: @selector( rightRevealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}
@end
